'use strict'

import express from 'express'
import { UserController } from './controllers/UserController'
import { Validator } from './middleware/validators'
import { AuthController } from './controllers/AuthController'
import { Policy } from './middleware/policies'

const router = express.Router()

router.get('/info', UserController.getInfo)
router.post('/register', [Validator.register, Policy.register], AuthController.register)

export { router }

'use strict'

import jwt from 'jsonwebtoken'

const generateUserToken = user => {
  try {
    return jwt.sign(user, getEnv('JWT_SECRET'), { expiresIn: getEnv('JWT_EXPIRE_SEC') })
  } catch (error) {
    throw error
  }
}

export { generateUserToken }

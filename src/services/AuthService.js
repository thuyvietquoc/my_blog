'user strict'

import { hashPassword, comparePassword } from '../utils/encryption'
import { Author } from '../models/schema/Author'
import { jsonSuccess, jsonError, errors } from '../utils/system'

class AuthService {
  static login = async (username, password) => {
    try {
      const user = await Author.findOne({ username })
      if (!user) {
        return null
      }
      const dbPassword = user.password
      const resultComparePass = await comparePassword(password, dbPassword)
      const isCorrectPassword = resultComparePass.result
      if (!isCorrectPassword) return null
      delete user.password
      return user
    } catch (error) {
      throw error
    }
  }

  static register = async (email, password) => {
    try {
      const hashPass = await hashPassword(password)

      if (!hashPass.success) return jsonError(errors.HASH_PASSWORD_ERROR)
      const data = {
        email,
        password: hashPass.result
      }
      await Author.create(data)
      return jsonSuccess()
    } catch (error) {
      console.log('ERORR: ', error)
      throw error
    }
  }
}

export { AuthService }

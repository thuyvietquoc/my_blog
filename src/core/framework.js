import { UserController } from '../controllers/UserController'

import { UserService } from '../services/UserService'

import { m as userSchema } from '../models/schema/Author'

const environments = ['LCL', 'PRO', 'STG', 'DEV']
const controllers = { UserController }
const services = { UserService }
const schemas = {
  userSchema
}

export {
  environments, controllers, services, schemas
}

'use strict'

// import path from 'path'
// import Sequelize from 'sequelize'
// import { exec } from 'child_process'
import mongoose from 'mongoose'
import { errors, jsonError, jsonSuccess, logger } from '../utils/system'
import { schemas } from './framework'

const preBoot = async () => {
  // -- this boot runs before any services' boot, we can connect database here

  // -- connect to mongodb
  logger.verbose('Connecting to mongodb...')
  let mongoResult = await new Promise(resolve => {
    mongoose.connect(`mongodb://${getEnv('MONGO_HOST')}:${getEnv('MONGO_PORT')}/${getEnv('MONGO_DB')}`, { useNewUrlParser: true })
      .then((mongo) => {
        return resolve(jsonSuccess(mongo))
      })
      .catch(err => {
        logger.error('Failed to connect to mongodb', err)
        return resolve(jsonError(errors.SYSTEM_ERROR))
      })
  })
  if (!mongoResult.success) { return mongoResult }
  logger.verbose('Connected to mongodb')

  // -- load models
  logger.verbose('Loading models...')
  let keys = Object.keys(schemas)
  for (let i = 0; i < keys.length; i++) {
    logger.verbose(`Loading schema ${keys[i]}...`)
    // -- in reality we either use mongo or sequelize, not both, do we don't check
    // the function name
    let schema = schemas[keys[i]]
    if (!schema) {
      logger.error(`Cannot load ${keys[i]}, please make sure you include the schema in framework`)
      return jsonError(errors.SYSTEM_ERROR)
    }
    schema(mongoResult.result, mongoose)
  }

  return jsonSuccess()
}
const boot = async () => {
  // -- this boot runs after all services had successfully booted
  return jsonSuccess()
}
const preExit = async () => {
  // -- this exit runs before any services' exit
  return jsonSuccess()
}
const exit = async () => {
  // -- this exit runs after all services had exited
  return jsonSuccess()
}

export { preBoot, boot, preExit, exit }

'use strict'

let Author
const m = (builder, Mongoose) => {
  Author = builder.model('Author', new Mongoose.Schema({
    email: { type: String, required: true },
    password: { type: String, required: true, select: false },
    verifyEmail: { type: Boolean, default: false },
    name: { type: String, required: false },
    avatar: { type: String, required: false },
    description: { type: String, required: false },
    createAt: { type: Date, required: true, default: Date.now },
    updateAt: { type: Date, required: true, default: Date.now }
  }))
}

export { Author, m }

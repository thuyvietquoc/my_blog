'use strict'

let Post
const m = (builder, Mongoose) => {
  Post = builder.model('Post', new Mongoose.Schema({
    status: { type: String, required: false },
    category: { type: Array, default: [] },
    likeCount: { type: String, required: false },
    commentCount: { type: String, required: false },

    articleTitle: { type: String, required: false },
    articleContent: { type: String, required: false },
    isDeleted: { type: Boolean, default: false },
    authorId: { type: Mongoose.Schema.ObjectId, ref: 'Author' },

    createAt: { type: Date, required: true, default: Date.now },
    updateAt: { type: Date, required: true, default: Date.now }
  }))
}

export { Post, m }

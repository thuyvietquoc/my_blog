'use strict'

let Comment
const m = (builder, Mongoose) => {
  Post = builder.model('Comment', new Mongoose.Schema({
    content: { type: String, required: false },
    type: { type: String, required: false },
    isDeleted: { type: Boolean, default: false },
    commenterId: { type: Mongoose.Schema.ObjectId, ref: 'Author' },
    postId: { type: Mongoose.Schema.ObjectId, ref: 'Post' },

    createAt: { type: Date, required: true, default: Date.now },
    updateAt: { type: Date, required: true, default: Date.now }
  }))
}

export { Comment, m }

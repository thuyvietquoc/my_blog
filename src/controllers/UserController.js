import { jsonSuccess, asyncWrap } from '../utils/system'

class UserController {
  static getInfo = asyncWrap(async (req, res) => {
    return res.json(jsonSuccess())
  })
}

// --- export the controller and mount at the mount point
export { UserController }

'use strict'

import { errors, jsonError, jsonSuccess, asyncWrap } from '../utils/system'
import { AuthService } from '../services/AuthService'
import { generateUserToken } from '../utils/commonFunctions'

class AuthController {
  static login = asyncWrap(async (req, res) => {
    const { username, password } = req.body
    const user = await AuthService.login(username, password)
    if (!user) return res.json(jsonError(errors.USER_NOT_FOUND))
    const tokenData = {
      id: user._id
    }
    const token = await generateUserToken(tokenData)
    if (!token) return res.json(jsonError(errors.TOKEN_GENERATED_FAILURE))
    return res.json(jsonSuccess({ token: token }))
  })

  static register = asyncWrap(async (req, res) => {
    const { email, password } = req.body

    const registerJson = await AuthService.register(email, password)

    return res.json(registerJson)
  })
}

export { AuthController }

import { errors, jsonError } from '../utils/system'
import { Author } from '../models/schema/Author'

class Policy {
  static async register (req, res, next) {
    const email = req.body.email.trim()
    const isEmailExist = await Author.findOne({ email })

    if (isEmailExist) {
      return res.json(jsonError(errors.AUTHOR_IS_EXSTED))
    }
    next()
  }

  static authenticated (req, res, next) {
    if (!req.principal || !req.principal.owner) { return res.json(jsonError(errors.NOT_AUTHENTICATED_ERROR)) }
    return next()
  }
}

export { Policy }

'use strict'

import Joi from 'joi'
import { jsonError } from '../utils/system'

class Validator {
  static register (req, res, next) {
    const { email, password } = req.body
    const schema = Joi.object().keys({
      email: Joi.string().email().required(),
      password: Joi.string().min(6).required().strip()
    })
    const result = Joi.validate({ email, password }, schema)
    if (result.error) {
      return res.json(jsonError(result.error.message))
    }
    return next()
  }
}

export { Validator }
